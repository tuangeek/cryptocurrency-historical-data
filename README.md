# cryptocurrency historical data (Open, High, Low, Close)

This dataset contains the historical trading data (OHLC) of more than 400 trading pairs at 1 minute resolution reaching back until the year 2013.

# Features
- historical trading data (OHLC)
- 400+ trading pairs (btcusd , ethusd, btceth, ...)
- 1 minute resolution
- Data 2013 - Present
- CSV files
- bitfinex api

## Installation

### Requirements

python3+

### Install Dependencies

```sh
pip install requests pip install aiohttp[speedups] pandas
```

Or via requirements.txt:
```sh
pip install -r requirements.txt
```

## Quickstart

### Running the scraper

This will get the last timestamp of each row of each pair.

```sh
python get_data.py
```

# Direct Link

The data is also cloned and can be downloaded directly from _https://data.quantcoin.io/data/{coin_pair}.csv.gz

### Examples Of Direct Link

BTCUSD

[btcusd.csv](https://data.quantcoin.io/data/btcusd.csv.gz) the link will be `https://data.quantcoin.io/data/btcusd.csv.gz`

ETHUSD

[ethusd.csv](https://data.quantcoin.io/data/ethusd.csv.gz) the link will be `https://data.quantcoin.io/data/ethusd.csv.gz`

ETHBTC

[ethbtc.csv](https://data.quantcoin.io/data/ethbtc.csv.gz) the link will be `https://data.quantcoin.io/data/ethbtc.csv.gz`

ETHUSDT

[ethust.csv](https://data.quantcoin.io/data/ethust.csv.gz) the link will be `https://data.quantcoin.io/data/ethust.csv.gz`

# Data Format

Each csv is name in pairs. So `btcusd.csv` is *BTC* value relative to *USD*

### btcusd.csv

BTC:USD

head

```sh
                open   close    high     low     volume
time                                                    
1364774820000   93.25   93.30   93.30   93.25  93.300000
1364774880000  100.00  100.00  100.00  100.00  93.300000
1364774940000   93.30   93.30   93.30   93.30  33.676862
1364775060000   93.35   93.47   93.47   93.35  20.000000
1364775120000   93.47   93.47   93.47   93.47   2.021627
```
tail

```sh
                       open         close     high         low      volume
time                                                                      
1606559820000  17225.000000  17264.000000  17283.0  17225.0000  102.395898
1606559880000  17263.000000  17236.000000  17263.0  17234.0000    4.957733
1606559940000  17234.984877  17258.000000  17258.0  17233.0000    0.567398
1606560000000  17258.266880  17259.870749  17272.0  17248.6576    1.379061
1606560060000  17261.305137  17270.000000  17270.0  17261.0000    1.056032
```

### ethbtc.csv

ETH:BTC

head

```sh
                  open    close     high      low  volume
time                                                     
1457539800000  0.02594  0.02594  0.02594  0.02594   0.100
1457547300000  0.02577  0.02577  0.02577  0.02577   0.010
1457550240000  0.02550  0.02530  0.02550  0.02520   3.264
1457554620000  0.02570  0.02570  0.02570  0.02570   0.010
1457561700000  0.02690  0.02790  0.02790  0.02630  20.000
```

tail

```sh
                   open     close      high       low     volume
time                                                            
1606559280000  0.030193  0.030116  0.030193  0.030116   0.650000
1606559760000  0.030101  0.030052  0.030101  0.030052  11.273705
1606559820000  0.030049  0.030039  0.030049  0.030039   1.930000
1606559880000  0.030017  0.029988  0.030017  0.029988   3.752337
1606559940000  0.029989  0.029989  0.029989  0.029989   0.008823
```

## Contributing

1. Fork it ( https://gitlab.com/tuangeek/cryptocurrency-historical-data )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Credits

Original script/idea from [akcarsten/bitfinex_api] (https://github.com/akcarsten/bitfinex_api)


## Warning

### Past Performance Is No Guarantee of Future Results
