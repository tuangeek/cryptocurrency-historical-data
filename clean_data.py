# Use for cleaning up some of the data converting csv with index by id and index by time
import pandas as pd
import os

data_dir = os.path.join(os.getcwd(), "data")
files = os.listdir(data_dir)

for file_name in files:
    if ".gzip" not in file_name:
        file_path = os.path.join(data_dir, file_name)
        df = pd.read_csv(
            file_path, index_col=[0], parse_dates=True, infer_datetime_format=True
        )
        df.rename(columns={"open": "Open", "high": "High", "low": "Low", "close": "Close",
                           "volume": "Volume"}, inplace=True)

        # change the file to .gz
        file_path = file_path + ".gz"
        if any(df.columns.str.match("time")):
            df.rename(columns={"time": "Time"}, inplace=True)
            df.to_csv(file_path, compression="gzip", index=False)
        else:
            df.index.names = ['Time']
            df.to_csv(file_path, compression="gzip")